/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdufay <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/13 13:24:53 by gdufay            #+#    #+#             */
/*   Updated: 2019/05/21 12:23:57 by gdufay           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_putnbr_pos(size_t n)
{
	if (n < 10)
		ft_putchar((char)n + '0');
	else
	{
		ft_putnbr_pos(n / 10);
		ft_putnbr_pos(n % 10);
	}
}

void	ft_putnbr(int c)
{
	unsigned int n;

	n = (unsigned int)c;
	if (c < 0)
	{
		n = (unsigned int)-c;
		ft_putchar('-');
	}
	if (n < 10)
		ft_putchar((char)n + '0');
	else
	{
		ft_putnbr(n / 10);
		ft_putnbr(n % 10);
	}
}
