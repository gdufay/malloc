/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putptr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdufay <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/13 13:25:00 by gdufay            #+#    #+#             */
/*   Updated: 2019/05/17 13:33:30 by gdufay           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_putptr(void *p)
{
	size_t	ptr;
	char	buf[42];
	int		i;

	ptr = (size_t)p;
	if (!ptr)
	{
		ft_putstr("0x0");
		return ;
	}
	ft_putstr("0x");
	i = 0;
	while (ptr > 0)
	{
		buf[i++] = ptr % 16 > 9
			? (char)(ptr % 16 - 10 + 'a')
			: (char)(ptr % 16 + '0');
		ptr /= 16;
	}
	while (--i >= 0)
		ft_putchar(buf[i]);
}
