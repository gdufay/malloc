/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libft.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdufay <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/07 13:58:07 by gdufay            #+#    #+#             */
/*   Updated: 2019/05/17 13:00:13 by gdufay           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFT_H
# define LIBFT_H

# include <stdlib.h>
# include <unistd.h>
# include <limits.h>

size_t		ft_strlen(const char *s);
void		ft_putstr(const char *s);
void		ft_putptr(void *p);
void		ft_putnbr(int n);
void		ft_putnbr_pos(size_t n);
void		ft_putchar(char c);
void		*ft_memcpy(void *dest, const void *src, size_t len);
void		*ft_memset(void *dest, int c, size_t len);

#endif
