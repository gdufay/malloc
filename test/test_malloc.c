#include <assert.h>
#include <stdlib.h>
#include <limits.h>
#include <string.h>
#include <unistd.h>
#include "malloc.h"

void ft_putstr(char *s)
{
	size_t len = strlen(s);
	write(1, s, len);
}

void test_neg1(void)
{
	assert(malloc(-1) == NULL);
	ft_putstr("test malloc(-1) réussi\n");
}

void test_intmin(void)
{
	assert(malloc(INT_MIN) == NULL);
	ft_putstr("test malloc(INT_MIN) réussi\n");
}

void test_sizemax(void)
{
	assert(malloc(ULLONG_MAX) == NULL);
	ft_putstr("test malloc(ULLONG_MAX) réussi\n");
}

void test_zero(void)
{
	assert(malloc(0) != NULL);
	ft_putstr("test malloc(0) réussi\n");
}

int main(void) {
	ft_putstr("--- TEST MALLOC ---\n");
	ft_putstr("\n--- DOIT RETOURNER NULL ---\n");

	test_neg1();
	test_intmin();
	test_sizemax();

	ft_putstr("\n--- DOIT PAS RETOURNER NULL ---\n");
	test_zero();

	ft_putstr("\n--- TEST MALLOC FINI ---\n");
	return (0);
}
