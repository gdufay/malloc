#include <assert.h>

#define M 1024 * 1024
#define NULL (void*)0

void	ptr_null_size_0(void)
{
	void	*ptr;

	ptr = realloc(NULL, 0);
	ft_putstr("ptr_null_size_0 should show 1 tiny alloc of 16\n");
	show_alloc_mem();
	ft_putstr("\n");
	free(ptr);
}

void	ptr_null_size_10(void)
{
	void	*ptr;

	ptr = realloc(NULL, 10);
	ft_putstr("ptr_null_size_10 should show 1 tiny alloc of 16\n");
	show_alloc_mem();
	ft_putstr("\n");
	free(ptr);
}

void	ptr_null_size_10000(void)
{
	void	*ptr;

	ptr = realloc(NULL, 10000);
	ft_putstr("ptr_null_size_10 should show 1 small alloc\n");
	show_alloc_mem();
	ft_putstr("\n");
	free(ptr);
}

void	ptr_null_size_10M(void)
{
	void	*ptr;

	ptr = realloc(NULL, 10 * M);
	ft_putstr("ptr_null_size_10M should show 1 large alloc\n");
	show_alloc_mem();
	ft_putstr("\n");
	free(ptr);
}

void	ptr_undef_size_0(void)
{
	void	*ptr;

	ptr = realloc("test", 0);
	ft_putstr("ptr_undef_size_0 should show nothing\n");
	show_alloc_mem();
	ft_putstr("\n");
	assert(ptr == NULL);
}

void	ptr_undef_size_10(void)
{
	void	*ptr;

	ptr = realloc("test", 10);
	ft_putstr("ptr_undef_size_10 should show nothing\n");
	show_alloc_mem();
	ft_putstr("\n");
	assert(ptr == NULL);
}

void	ptr_undef_size_10000(void)
{
	void	*ptr;

	ptr = realloc("test", 10000);
	ft_putstr("ptr_undef_size_10 should show nothing\n");
	show_alloc_mem();
	ft_putstr("\n");
	assert(ptr == NULL);
}

void	ptr_undef_size_10M(void)
{
	void	*ptr;

	ptr = realloc("test", 10 * M);
	ft_putstr("ptr_undef_size_10M should show nothing\n");
	show_alloc_mem();
	ft_putstr("\n");
	assert(ptr == NULL);
}

void	ptr_malloc0_size_0(void)
{
	void	*ptr;

	ptr = malloc(0);
	ptr = realloc(ptr, 0);
	ft_putstr("ptr_malloc0_size_0 should show 1 tiny alloc of 16\n");
	show_alloc_mem();
	ft_putstr("\n");
	free(ptr);
}

void	ptr_malloc0_size_10(void)
{
	void	*ptr;

	ptr = malloc(0);
	ptr = realloc(ptr, 10);
	ft_putstr("ptr_malloc0_size_10 should show 1 tiny alloc of 16\n");
	show_alloc_mem();
	ft_putstr("\n");
	free(ptr);
}

void	ptr_malloc10_size_0(void)
{
	void	*ptr;

	ptr = malloc(10);
	ptr = realloc(ptr, 0);
	ft_putstr("ptr_malloc10_size_0 should show 1 tiny alloc of 16\n");
	show_alloc_mem();
	ft_putstr("\n");
	free(ptr);
}

void	ptr_malloc10_size_10(void)
{
	void	*ptr;

	ptr = malloc(10);
	ptr = realloc(ptr, 10);
	ft_putstr("ptr_malloc10_size_10 should show 1 tiny alloc of 16\n");
	show_alloc_mem();
	ft_putstr("\n");
	free(ptr);
}

void	ptr_malloc10_size_100(void)
{
	void	*ptr;

	ptr = malloc(10);
	ptr = realloc(ptr, 100);
	ft_putstr("ptr_malloc10_size_100 should show 1 tiny alloc of 112\n");
	show_alloc_mem();
	ft_putstr("\n");
	free(ptr);
}

int		main(void)
{
	ptr_null_size_0();
	ptr_null_size_10();
	ptr_null_size_10000();
	ptr_null_size_10M();

	ptr_undef_size_0();
	ptr_undef_size_10();
	ptr_undef_size_10000();
	ptr_undef_size_10M();

	ptr_malloc0_size_0();
	ptr_malloc0_size_10();
	ptr_malloc10_size_0();
	ptr_malloc10_size_10();
	ptr_malloc10_size_100();

	// loop
	// test ptr free size 0
	// test ptr free size > 0
	// test ptr realloc null size > 0 realloc size <
	// test ptr realloc null size > 0 realloc size >
	return (0);
}
