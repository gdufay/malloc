#include <assert.h>
#include <limits.h>

int main(void)
{
	assert(malloc_good_size(0) == 16);
	assert(malloc_good_size(10) == 16);
	assert(malloc_good_size(16) == 16);
	assert(malloc_good_size(1) == 16);
	assert(malloc_good_size(12) == 16);
	assert(malloc_good_size(24) == 32);
	assert(malloc_good_size(25) == 32);
	assert(malloc_good_size(1009) == 1024);
	assert(malloc_good_size(1025) == 1536);
	assert(malloc_good_size(10025) == 10240);
	assert(malloc_good_size(-0) == 16);
	assert(malloc_good_size(-10) == 0);
	return (0);
}
