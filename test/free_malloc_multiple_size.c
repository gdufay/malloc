#include <stdlib.h>

int main()
{
	char	*addr;

	ft_putstr("-- SHOULD SHOW NOTHING\n\n");
	addr = (char*)malloc(5);
	free(addr);
	addr = (char*)malloc(5);
	free(addr);
	addr = (char*)malloc(15);
	free(addr);
	addr = (char*)malloc(150);
	free(addr);
	addr = (char*)malloc(3200);
	free(addr);
	addr = (char*)malloc(32000);
	free(addr);
	addr = (char*)malloc(420000);
	free(addr);
	addr = (char*)malloc(4200000);
	free(addr);
	show_alloc_mem();
	return (0);
}
