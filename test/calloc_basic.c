#include <limits.h>
#include <assert.h>
#include <string.h>

# define NULL (void*)0

void	count_max_size_max(void)
{
	assert(calloc(ULLONG_MAX, ULLONG_MAX) == NULL);
	ft_putstr("count_max_size_max OK\n");
}

void	count_10_size_max(void)
{
	assert(calloc(10, ULLONG_MAX) == NULL);
	ft_putstr("count_10_size_max OK\n");
}

void	count_max_size_10(void)
{
	assert(calloc(ULLONG_MAX, 10) == NULL);
	ft_putstr("count_max_size_10 OK\n");
}

void	count_neg_size_neg(void)
{
	assert(calloc(-1, -15) == NULL);
	ft_putstr("count_neg_size_neg OK\n");
}

void	display_hello(void)
{
	char	*ptr;

	ptr = (char*)calloc(1, 7);
	strcpy(ptr, "Hello\n");
	ft_putstr(ptr);
}

int main(void)
{
	count_max_size_max();
	count_10_size_max();
	count_max_size_10();
	count_neg_size_neg();

	display_hello();
	return (0);
}
