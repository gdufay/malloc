#include <stdlib.h>

int main()
{
	char	*addr1;
	char	*addr2;
	char	*addr3;
	char	*addr4;

	ft_putstr("-- SHOULD 2 TINY\n\n");
	addr1 = (char*)malloc(5);
	addr2 = (char*)malloc(5);
	addr3 = (char*)malloc(15);
	free(addr2);
	free(addr3);
	addr4 = (char*)malloc(150);
	show_alloc_mem();
	free(addr1);
	free(addr4);

	ft_putstr("\n-- SHOULD 2 SMALL\n");
	addr1 = (char*)malloc(1024);
	addr2 = (char*)malloc(1024);
	addr3 = (char*)malloc(3024);
	free(addr2);
	free(addr3);
	addr4 = (char*)malloc(3150);
	show_alloc_mem();
	free(addr1);
	free(addr4);

	ft_putstr("\n-- SHOULD 2 LARGE\n");
	addr1 = (char*)malloc(16 * 1000 * 1000);
	addr2 = (char*)malloc(17 * 1000 * 1000);
	addr3 = (char*)malloc(18 * 1000 * 1000);
	free(addr2);
	free(addr3);
	addr4 = (char*)malloc(19 * 1000 * 1000);
	show_alloc_mem();
	free(addr1);
	free(addr4);
	return (0);
}
