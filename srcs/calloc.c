/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   calloc.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdufay <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/17 12:13:15 by gdufay            #+#    #+#             */
/*   Updated: 2019/05/23 15:00:06 by gdufay           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc.h"

void	*calloc(size_t count, size_t size)
{
	size_t	alloc;
	void	*ptr;

	alloc = size * count;
	if ((count && size != alloc / count) || (size && count != alloc / size))
		return (NULL);
	if (!(ptr = malloc(alloc)))
		return (NULL);
	alloc = malloc_good_size(alloc);
	if (pthread_mutex_lock(&g_mut))
		return (NULL);
	ft_memset(ptr, 0, alloc);
	pthread_mutex_unlock(&g_mut);
	return (ptr);
}
