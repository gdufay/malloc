/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   free.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdufay <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/13 13:23:56 by gdufay            #+#    #+#             */
/*   Updated: 2019/05/23 14:44:43 by gdufay           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc.h"

static size_t	tmp2(size_t size)
{
	return ((size > SMALL ? size : size / getpagesize()) + BLOCK_SIZE);
}

static void		unmap(t_block *block)
{
	t_block		**head;
	t_block		*tmp;

	head = get_block_head(tmp2(block->size));
	tmp = *head;
	while (tmp->next && tmp->next != block)
		tmp = tmp->next;
	if (tmp->next)
		tmp->next = block->next;
	if (tmp->next)
		tmp->next->prev = tmp;
	if (block == *head)
		*head = (*head)->next;
	if (munmap(block, block->size + BLOCK_SIZE) == -1)
		ft_putstr("munmap error\n");
}

static void		locked_free(void *ptr)
{
	t_block		*block;

	if (!ptr || !(block = (t_block*)ptr - 1)
			|| !check_block(block, is_same_ptr) || block->free)
		return ;
	if (is_unmapable(block))
	{
		unmap(block);
		return ;
	}
	block->free = 1;
	if (block->next && block->next->free)
		merge_block(block, block->next);
	if (block->prev && block->prev->free)
		merge_block(block->prev, block);
}

void			free(void *ptr)
{
	if (pthread_mutex_lock(&g_mut))
		return ;
	locked_free(ptr);
	pthread_mutex_unlock(&g_mut);
}
