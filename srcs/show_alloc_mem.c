/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   show_alloc_mem.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdufay <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/13 13:24:20 by gdufay            #+#    #+#             */
/*   Updated: 2019/05/21 11:02:10 by gdufay           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc.h"

static void		print_block(t_block *block)
{
	ft_putptr(block + 1);
	ft_putstr(" - ");
	ft_putptr((char*)block + BLOCK_SIZE + block->size);
	ft_putstr(" : ");
	ft_putnbr((int)block->size);
	ft_putstr(" octets\n");
}

static size_t	print_and_count_block(t_block *block, size_t len)
{
	if (!block)
		return (len);
	if (block->free)
		return (print_and_count_block(block->next, len));
	print_block(block);
	return (print_and_count_block(block->next, len + block->size));
}

static char		*zone_name_map(size_t size)
{
	if (size == TINY)
		return ("TINY");
	else if (size == SMALL)
		return ("SMALL");
	else
		return ("LARGE");
}

static size_t	print_zone(t_block *head, size_t zone_len, size_t len)
{
	if (!head)
		return (len);
	if (head->next || !head->free)
	{
		ft_putstr(zone_name_map(zone_len));
		ft_putstr(" : ");
		ft_putptr(head);
		ft_putstr("\n");
	}
	return (print_and_count_block(head, len));
}

void			show_alloc_mem(void)
{
	size_t			total;
	int				i;
	static size_t	blocks[3] = {TINY, SMALL, SMALL + 1};

	if (pthread_mutex_lock(&g_mut))
		return ;
	i = -1;
	total = 0;
	while (++i < 3)
		total += print_zone(*get_block_head(blocks[i]), blocks[i], 0);
	ft_putstr("Total : ");
	ft_putnbr((int)total);
	ft_putstr(" octets\n");
	pthread_mutex_unlock(&g_mut);
}
