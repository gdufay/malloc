/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   malloc_size.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdufay <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/21 12:04:32 by gdufay            #+#    #+#             */
/*   Updated: 2019/05/23 14:50:04 by gdufay           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc.h"

size_t		malloc_good_size(size_t size)
{
	if (!size)
		size = 1;
	return (ALIGN(size));
}

size_t		malloc_size(void *ptr)
{
	t_block		*block;

	if (!ptr || !(block = (t_block*)ptr - 1))
		return (0);
	return (block->size);
}
