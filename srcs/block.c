/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   block.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdufay <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/15 11:44:01 by gdufay            #+#    #+#             */
/*   Updated: 2019/05/23 14:46:38 by gdufay           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc.h"

t_block		**get_block_head(size_t size)
{
	static t_block	*tiny = NULL;
	static t_block	*small = NULL;
	static t_block	*large = NULL;

	if (size <= TINY)
		return (&tiny);
	else if (size <= SMALL)
		return (&small);
	else
		return (&large);
}

t_block		*find_block(t_block *head, int (*f)(t_block*, void*), void *ptr)
{
	while (head)
	{
		if (f(head, ptr))
			return (head);
		head = head->next;
	}
	return (NULL);
}

void		split_block(t_block *block, size_t size)
{
	t_block	*new;

	if (!block || !size)
		return ;
	new = (t_block*)(void*)((char*)block + size + BLOCK_SIZE);
	new->size = block->size - size - BLOCK_SIZE;
	new->next = block->next;
	new->prev = block;
	new->free = 1;
	new->limit = 0;
	if (new->next)
		new->next->prev = new;
	block->size = size;
	block->next = new;
}

int			check_block(t_block *block, int (*f)(t_block*, void*))
{
	int				i;
	static size_t	blocks[3] = {TINY, SMALL, SMALL + 1};

	i = -1;
	while (++i < 3)
		if (find_block(*get_block_head(blocks[i]), f, block))
			return (1);
	return (0);
}

void		merge_block(t_block *a, t_block *b)
{
	if (b->limit)
		return ;
	a->size += b->size + BLOCK_SIZE;
	a->next = b->next;
	if (a->next)
		a->next->prev = a;
	b->prev = NULL;
	b->next = NULL;
}
