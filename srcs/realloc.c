/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   realloc.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdufay <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/13 13:24:05 by gdufay            #+#    #+#             */
/*   Updated: 2019/05/27 09:59:26 by gdufay           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc.h"

static void	*gain_de_place(void *ptr)
{
	pthread_mutex_unlock(&g_mut);
	free(ptr);
	return (malloc(0));
}

static void	*locked_realloc(void *ptr, size_t size)
{
	t_block		*block;
	void		*ret;

	if (!ptr)
	{
		pthread_mutex_unlock(&g_mut);
		return (malloc(size));
	}
	if (!(block = (t_block*)ptr - 1) || !check_block(block, is_same_ptr))
		return (NULL);
	if (!size)
		gain_de_place(ptr);
	if (block->size >= size)
		return (ptr);
	pthread_mutex_unlock(&g_mut);
	ret = malloc(size);
	if (pthread_mutex_lock(&g_mut))
		return (NULL);
	if (ret)
	{
		ft_memcpy(ret, ptr, block->size);
		pthread_mutex_unlock(&g_mut);
		free(ptr);
	}
	return (ret);
}

void		*realloc(void *ptr, size_t size)
{
	void	*ret;

	if (pthread_mutex_lock(&g_mut))
		return (NULL);
	ret = locked_realloc(ptr, size);
	pthread_mutex_unlock(&g_mut);
	return (ret);
}

void		*reallocf(void *ptr, size_t size)
{
	void	*ret;

	if (!(ret = realloc(ptr, size)))
		free(ptr);
	return (ret);
}
