/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   malloc.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdufay <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/13 13:23:46 by gdufay            #+#    #+#             */
/*   Updated: 2019/05/24 15:54:40 by gdufay           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc.h"

pthread_mutex_t g_mut = PTHREAD_MUTEX_INITIALIZER;

static t_block	*extend_heap(t_block **head, size_t size)
{
	t_block	*tmp;
	t_block	*first;
	size_t	alloc;

	alloc = size > SMALL ? size + BLOCK_SIZE
		: (size_t)(ZONE(size) * getpagesize());
	if ((first = (t_block*)mmap(NULL, alloc, PROT_READ | PROT_WRITE,
					MAP_ANON | MAP_PRIVATE, -1, 0)) == MAP_FAILED)
		return (NULL);
	first->free = 1;
	first->prev = NULL;
	first->next = NULL;
	first->size = alloc - BLOCK_SIZE;
	first->limit = 1;
	if (!*head)
		*head = first;
	else
	{
		tmp = *head;
		while (tmp->next)
			tmp = tmp->next;
		first->prev = tmp;
		tmp->next = first;
	}
	return (first);
}

static void		*locked_malloc(size_t size)
{
	t_block		**head;
	t_block		*block;

	if (size <= SMALL)
		size = malloc_good_size(size);
	head = get_block_head(size);
	block = find_block(*head, is_equal_and_free, (void*)size);
	if (!block)
		block = extend_heap(head, size);
	if (is_splittable(block, size))
		split_block(block, size);
	if (block)
		block->free = 0;
	return (block ? block + 1 : NULL);
}

void			*malloc(size_t size)
{
	void	*ret;

	if (pthread_mutex_lock(&g_mut))
		return (NULL);
	ret = locked_malloc(size);
	pthread_mutex_unlock(&g_mut);
	return (ret);
}
