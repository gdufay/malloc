/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   is_fct.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdufay <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/21 11:49:43 by gdufay            #+#    #+#             */
/*   Updated: 2019/05/23 11:06:58 by gdufay           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc.h"

int		is_same_ptr_not_free(t_block *block, void *ptr)
{
	return (block == (t_block*)ptr && block->free == 0);
}

int		is_unmapable(t_block *block)
{
	return (block->size > SMALL || (block->limit == 1 && block->prev
				&& (!block->next || block->next->limit)));
}

int		is_splittable(t_block *block, size_t size)
{
	return (block && block->size >= size + BLOCK_SIZE);
}

int		is_equal_and_free(t_block *block, void *ptr_size)
{
	size_t	size;

	if (!block || !ptr_size)
		return (0);
	size = (size_t)ptr_size;
	return (block->size >= size && block->free == 1);
}

int		is_same_ptr(t_block *block, void *ptr)
{
	return (block == (t_block*)ptr);
}
