/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   malloc.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gdufay <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/14 16:28:58 by gdufay            #+#    #+#             */
/*   Updated: 2019/05/27 10:04:46 by gdufay           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MALLOC_H
# define MALLOC_H

# include <sys/mman.h>
# include <unistd.h>
# include <pthread.h>

# include "../libft/includes/libft.h"

# define TINY 992
# define TINY_ZONE 512
# define SMALL 127 * 1024 - 1
# define SMALL_ZONE 4096
# define LARGE SMALL + 1

# define ALIGN16(x) ((((x - 1) >> 4)) << 4) + 16
# define ALIGN512(x) ((((x - 1) >> 9)) << 9) + 512
# define ALIGN(x) (x > TINY ? ALIGN512(x) : ALIGN16(x))

# define ZONE(x) (x > TINY ? SMALL_ZONE : TINY_ZONE)

# define TRUE 1
# define FALSE 0

extern pthread_mutex_t		g_mut;

typedef	struct				s_block
{
	size_t			size;
	struct s_block	*next;
	struct s_block	*prev;
	int				free;
	int				limit;
}							t_block;

# define BLOCK_SIZE sizeof(t_block)

void						*malloc(size_t size);
void						free(void *ptr);
void						*realloc(void *ptr, size_t size);
void						*reallocf(void *ptr, size_t size);
void						show_alloc_mem(void);
size_t						malloc_size(void *ptr);
size_t						malloc_good_size(size_t size);

t_block						**get_block_head(size_t size);
int							check_block(t_block *block,
		int (*f)(t_block*, void*));
t_block						*find_block(t_block *head,
		int (*f)(t_block*, void*), void *ptr);
void						split_block(t_block *block, size_t size);
void						merge_block(t_block *a, t_block *b);

int							is_same_ptr_not_free(t_block *block, void *ptr);
int							is_unmapable(t_block *block);
int							is_splittable(t_block *block, size_t size);
int							is_equal_and_free(t_block *block, void *ptr_size);
int							is_same_ptr(t_block *block, void *ptr);

#endif
