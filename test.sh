#!/bin/bash

basic_compile=("test0" "test1" "test2" "test3" "test3.2" "test4" "free_another_ptr" "free_null" "free_malloc_multiple_large_size" "malloc_multiple_large_size")
lib_compile=("test5" "double_free" "malloc_multiple_size" "merge_block" "free_malloc_multiple_size" "realloc_basic" "calloc_basic" "test_extend_heap" "malloc_good_size")

make re;

for var in "${basic_compile[@]}"
do
	gcc -o test/$var test/$var.c
done

for var in "${lib_compile[@]}"
do
	gcc -o test/$var test/$var.c -L. -lft_malloc 2> /dev/null
done

printf 'test0\n';
./run.sh /usr/bin/time -l ./test/test0;

printf '\ntest1\n';
./run.sh /usr/bin/time -l ./test/test1;

printf '\ntest2\n';
./run.sh /usr/bin/time -l ./test/test2;

printf '\ntest3\n';
./run.sh ./test/test3;

printf '\ntest3.2\n';
./run.sh ./test/test3.2;

printf '\ntest4\n';
./run.sh ./test/test4;

printf '\ntest5\n';
./test/test5;

printf '\ndouble_free\n';
./run.sh ./test/double_free;

printf '\nfree_another_ptr\n';
./run.sh ./test/free_another_ptr;

printf '\nfree_malloc_multiple_size\n';
./test/free_malloc_multiple_size;

printf '\nfree_null\n';
./run.sh ./test/free_null;

printf '\nmalloc_multiple_size\n';
./test/malloc_multiple_size;

printf '\nmerge_block\n';
./test/merge_block;

printf '\nrealloc_basic\n';
./test/realloc_basic;

printf '\ncalloc_basic\n';
./test/calloc_basic;

printf '\nmalloc_good_size\n';
./test/malloc_good_size;

printf '\nmalloc_multiple_large_size\n';
./run.sh /usr/bin/time -l ./test/malloc_multiple_large_size;

printf '\nfree_malloc_multiple_large_size\n';
./run.sh /usr/bin/time -l ./test/free_malloc_multiple_large_size;

for var in "${basic_compile[@]}" "${lib_compile[@]}"
do
	rm test/$var
done


printf '\nls\n';
./run.sh ls;

printf '\nls -G\n';
./run.sh ls -G;

printf '\ntest vim, ls -lisaRG ~, emacs, nano\n';
