NAME = libft_malloc_$(HOSTTYPE).so
NAME_LN = libft_malloc.so

ifeq ($(HOSTTYPE),)
	HOSTTYPE = $(shell uname -m)_$(shell uname -s)
endif

CC = gcc
CFLAGS = -Wall -Wextra -Werror -Wpadded -g
ICLD = -Iincludes/

INCLUDES = includes/malloc.h

SRCD = srcs/
_SRCS = malloc.c free.c show_alloc_mem.c block.c realloc.c calloc.c is_fct.c malloc_size.c
SRCS = $(addprefix $(SRCD), $(_SRCS))
OBJS = $(SRCS:.c=.o)

# COLORS
CRED=\033[91m
CGREEN=\033[38;2;0;255;145m
CEND=\033[0m

all: libft $(NAME)

libft:
	@make -s -C libft

$(NAME): $(OBJS)
	@printf "\r\033[K$(CGREEN)Creating library$(CEND): $(NAME)\n"
	@$(CC) -shared -o $@ $^ libft/libft.a
	@ln -s $@ $(NAME_LN) 
	@echo  "$(NAME): $(CGREEN)done$(CEND)"

%.o: %.c $(INCLUDES)
	@printf "\r\033[K$(CGREEN)Compiling$(CEND): $<"
	@$(CC) $(CFLAGS) -o $@ -c $< $(ICLD)

clean:
	@echo "$(CRED)Cleaning$(CEND): $(NAME)"
	@rm -f $(OBJS)
	@make -s -C libft clean

fclean: clean
	@echo "$(CRED)Full cleaning$(CEND): $(NAME)"
	@rm -f $(NAME) $(NAME_LN)
	@make -s -C libft fclean

re: fclean all


.PHONY: all clean fclean re libft
